import pomelo.createApp
import pomelo.version

fun main(args: Array<String>) {
    println("pomelo version: ${version}")
    val app = createApp()
    println("app: $app")
}


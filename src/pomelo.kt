@file:JsModule("pomelo")
package pomelo

external open class Application {
    open fun getBase(): String = definedExternally
    open fun filter(filter: Any): Unit = definedExternally
    open fun before(bf: Any): Unit = definedExternally
    open fun before(bf: (msg: Any, session: Any, next: Any) -> Any): Unit = definedExternally
    open fun after(af: Any): Unit = definedExternally
    open fun after(af: (err: Any, msg: Any, session: Any, resp: Any, next: Any) -> Any): Unit = definedExternally
    open fun globalFilter(filter: Any): Unit = definedExternally
    open fun globalBefore(bf: Any): Unit = definedExternally
    open fun globalBefore(bf: (msg: Any, session: Any, next: Any) -> Any): Unit = definedExternally
    open fun globalAfter(af: Any): Unit = definedExternally
    open fun globalAfter(af: (err: Any, msg: Any, session: Any, resp: Any, next: Any) -> Any): Unit = definedExternally
    open fun rpcBefore(bf: Any): Unit = definedExternally
    open fun rpcBefore(bf: (serverId: Any, msg: Any, opts: Any, next: Any) -> Any): Unit = definedExternally
    open fun rpcAfter(af: Any): Unit = definedExternally
    open fun rpcAfter(af: (serverId: Any, msg: Any, opts: Any, next: Any) -> Any): Unit = definedExternally
    open fun rpcFilter(filter: Any): Unit = definedExternally
    open fun load(name: String, component: Any, opts: Any? = definedExternally /* null */): Any = definedExternally
    open fun load(component: Any, opts: Any? = definedExternally /* null */): Any = definedExternally
    open fun loadConfig(key: String, `val`: String): Any = definedExternally
    open fun route(kserverTypeey: String, routeFunc: (session: Any, msg: Any, app: Any, cb: Any) -> Any): Any = definedExternally
    open fun beforeStopHook(`fun`: Function<*>): Unit = definedExternally
    open fun start(cb: Function<*>? = definedExternally /* null */): Unit = definedExternally
    open fun set(setting: String, `val`: String, attach: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun set(setting: String, `val`: Any, attach: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun get(setting: String): dynamic /* String | Any */ = definedExternally
    open fun enabled(setting: String): Boolean = definedExternally
    open fun disabled(setting: String): Boolean = definedExternally
    open fun enable(setting: String): Application = definedExternally
    open fun disable(setting: String): Application = definedExternally
    open fun configure(env: String, type: String, fn: () -> Any): Application = definedExternally
    open fun configure(env: String, fn: () -> Any): Application = definedExternally
    open fun configure(fn: () -> Any): Application = definedExternally
    open fun registerAdmin(moduleId: String, module: Any, opts: Any): Unit = definedExternally
    open fun use(plugin: Any, opts: Any? = definedExternally /* null */): Unit = definedExternally
    open fun transaction(name: String, conditions: Any, handlers: Any, retry: Number): Unit = definedExternally
    open fun getMaster(): MasterInfo = definedExternally
    open fun getCurServer(): ServerInfo = definedExternally
    open fun getServerId(): String = definedExternally
    open fun getServerType(): String = definedExternally
    open fun getServers(): Any = definedExternally
    open fun getServersFromConfig(): Any = definedExternally
    open fun getServerTypes(): Array<String> = definedExternally
    open fun getServerById(serverId: String): ServerInfo = definedExternally
    open fun getServerFromConfig(serverId: String): ServerInfo = definedExternally
    open fun getServersByType(serverType: String): Array<ServerInfo> = definedExternally
    open fun isFrontend(server: ServerInfo): Boolean = definedExternally
    open fun isBackend(server: ServerInfo): Boolean = definedExternally
    open fun isMaster(): Boolean = definedExternally
    open fun addServers(servers: Array<ServerInfo>): Unit = definedExternally
    open fun removeServers(servers: Array<String>): Unit = definedExternally
    open fun replaceServers(servers: Any): Unit = definedExternally
    open fun addCrons(crons: Any): Unit = definedExternally
    open fun removeCrons(crons: Any): Unit = definedExternally
    open var rpc: Any = definedExternally
    open var backendSessionService: BackendSessionService = definedExternally
    open var sessionService: SessionService = definedExternally
}
external open class BackendSessionService {
    open fun get(frontendId: String, sid: String, cb: Function<*>): Unit = definedExternally
    open fun getByUid(frontendId: String, uid: String, cb: Function<*>): Unit = definedExternally
    open fun kickBySid(frontendId: String, sid: String, cb: Function<*>): Unit = definedExternally
    open fun kickByUid(frontendId: String, uid: String, reason: String, cb: Function<*>): Unit = definedExternally
}
external open class BackendSession {
    open fun bind(uid: String, cb: Function<*>): Unit = definedExternally
    open fun unbind(uid: String, cb: Function<*>): Unit = definedExternally
    open fun set(key: String, value: Any): Unit = definedExternally
    open fun get(key: String): Any = definedExternally
    open fun push(key: String, cb: Function<*>): Unit = definedExternally
    open fun pushAll(cb: Function<*>): Unit = definedExternally
}
external open class ChannelService {
    open fun createChannel(name: String): Channel = definedExternally
    open fun getChannel(name: String, create: Boolean): Channel = definedExternally
    open fun destroyChannel(name: String): Unit = definedExternally
    open fun pushMessageByUids(route: String, msg: Any, uids: Any, opts: Any? = definedExternally /* null */, cb: ((err: Any) -> Unit)? = definedExternally /* null */): Unit = definedExternally
    open fun broadcast(stype: String, route: String, msg: Any, opts: Any? = definedExternally /* null */, cb: Function<*>? = definedExternally /* null */): Unit = definedExternally
}
external open class Channel {
    open fun add(uid: String, sid: String): Boolean = definedExternally
    open fun leave(uid: String, sid: String): Boolean = definedExternally
    open fun getMembers(): Any = definedExternally
    open fun getMember(uid: String): Any = definedExternally
    open fun destroy(): Unit = definedExternally
    open fun pushMessage(route: String, msg: Any, opts: Any? = definedExternally /* null */, cb: Function<*>? = definedExternally /* null */): Unit = definedExternally
    open fun pushMessage(msg: Any, opts: Any? = definedExternally /* null */, cb: Function<*>? = definedExternally /* null */): Unit = definedExternally
}
external open class SessionService {
    open fun getByUid(uid: String): Array<dynamic /* Session | FrontendSession */> = definedExternally
    open fun kick(uid: String, cb: Function<*>): Unit = definedExternally
    open fun kickBySessionId(sid: String, cb: Function<*>): Unit = definedExternally
}
external open class Session {
    open fun bind(uid: String): Unit = definedExternally
    open fun unbind(uid: String): Unit = definedExternally
    open fun set(key: String, value: Any): Unit = definedExternally
    open fun set(key: Any, value: Any): Unit = definedExternally
    open fun remove(key: String): Unit = definedExternally
    open fun get(key: String): Any = definedExternally
    open fun send(msg: Any): Unit = definedExternally
    open fun sendBatch(msgs: Array<Any>): Unit = definedExternally
}
external open class FrontendSession {
    open var uid: String = definedExternally
    open fun bind(uid: String, cb: Function<*>? = definedExternally /* null */): Unit = definedExternally
    open fun unbind(uid: String, cb: Function<*>? = definedExternally /* null */): Unit = definedExternally
    open fun set(key: String, value: Any): Unit = definedExternally
    open fun set(key: Any, value: Any): Unit = definedExternally
    open fun get(key: String): Any = definedExternally
    open fun push(key: String, cb: (err: Error) -> Unit): Unit = definedExternally
    open fun on(event: String, listener: Function<*>): Unit = definedExternally
}
external open class MasterInfo {
    open var id: String = definedExternally
    open var host: Any = definedExternally
    open var port: Any = definedExternally
}
external open class ServerInfo {
    open var id: String = definedExternally
    open var serverType: String = definedExternally
    open var host: String = definedExternally
    open var port: Number = definedExternally
    open var clientPort: Number = definedExternally
}
external open class Connector {
    open var sioconnector: Any = definedExternally
    open var hybridconnector: Any = definedExternally
    open var udpconnector: Any = definedExternally
    open var mqttconnector: Any = definedExternally
}
external open class PushScheduler {
    open var direct: Any = definedExternally
    open var buffer: Any = definedExternally
}
external var version: Any = definedExternally
external var events: Any = definedExternally
external var components: Any = definedExternally
external var filters: Any = definedExternally
external var rpcFilters: Any = definedExternally
external var connectors: Connector = definedExternally
external var pushSchedulers: PushScheduler = definedExternally
external fun createApp(): Application = definedExternally
external fun timeout(): Any = definedExternally

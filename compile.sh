#!/bin/sh

run() {
  echo $1
  $1
}

run "kotlinc-js -output demo.js -module-kind commonjs src"
run "node demo.js"

